Bootstrap: docker
From: ubuntu:22.04

%environment
  export PATH=/usr/lib/rstudio-server/bin:$PATH

%post -c /bin/bash
  TOOL_VERSION=2023.12.1-402
  
  # Install system dependencies
  apt-get -qq update

  # Add en_US.UTF-8 locale
  apt-get -qq install locales
  locale-gen --purge en_US.UTF-8
  echo -e 'LANG="en_US.UTF-8"\nLANGUAGE="en_US:en"\n' > /etc/default/locale

  export DEBIAN_FRONTEND=noninteractive
  ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
  apt-get -qq install -y tzdata
  dpkg-reconfigure --frontend noninteractive tzdata

  apt-get -qq install -y \
    r-base \
    libssl3 \
    gdebi-core \
    wget \
    git

  # Download and install rstudio-server
  wget -q wget https://download2.rstudio.org/server/jammy/amd64/rstudio-server-${TOOL_VERSION}-amd64.deb
  gdebi -n rstudio-server-${TOOL_VERSION}-amd64.deb

  # Cleaning
  apt-get -qq autoremove -y
  apt-get -qq clean && rm -rf /var/lib/apt/lists/*
  rm rstudio-server-${TOOL_VERSION}-amd64.deb

%labels
  Author Julien Seiler <seilerj@igbmc.fr>

%test
  rserver --help
