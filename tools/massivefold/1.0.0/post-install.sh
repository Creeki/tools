#!/bin/bash
  
version=1.0.0

set +e

eval "$(${CONDA_HOME}/bin/conda shell.bash hook)"

conda activate massivefold-${version}
if [ $? -ne 0 ]
then
    conda activate ${CONDA_HOME}/envs/massivefold-${version}
fi

if [ 'massivefold-'${version} != "${CONDA_DEFAULT_ENV}" ]
then
    echo "Unable to load massivefold env"
    exit 1;
fi

set -e

# download & apply patch
cd ${CONDA_PREFIX}/lib/python3.8/site-packages/
wget -N https://raw.githubusercontent.com/GBLille/MassiveFold/MFv${version}/docker/openmm.patch
patch -p0 -N < openmm.patch || true

# recompile patched lib
cd ${CONDA_PREFIX}/lib/python3.8/site-packages/simtk/openmm/app/
python -m compileall .

# get chemical props
cd ${CONDA_PREFIX}/lib/python3.8/site-packages/alphafold/common/
wget -N https://git.scicore.unibas.ch/schwede/openstructure/-/raw/7102c63615b64735c4941278d92b554ec94415f8/modules/mol/alg/src/stereo_chemical_props.txt

# get launcher
cd ${CONDA_PREFIX}/bin/
wget -N https://raw.githubusercontent.com/GBLille/MassiveFold/MFv${version}/run_alphafold.py

#create run_alphafold.sh
echo '#!/bin/bash

python '${CONDA_PREFIX}'/bin/run_alphafold.py "$@"' > ${CONDA_PREFIX}/bin/run_alphafold.sh

chmod +x ${CONDA_PREFIX}/bin/run_alphafold.sh

