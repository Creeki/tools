#!/bin/bash

version=3.0.0

set +e

eval "$(${CONDA_HOME}/bin/conda shell.bash hook)"

conda activate culebront-${version}
if [ $? -ne 0 ]
then
    conda activate ${CONDA_HOME}/envs/culebront-${version}
fi

if [ 'culebront-'${version} != "${CONDA_DEFAULT_ENV}" ]
then
    echo "Unable to load culebront env"
    exit 1;
fi

set -e

# run install cluster command line
culebrONT install_cluster -s slurm -e modules
