BootStrap: docker
From: nvidia/cuda:11.4.0-base-ubuntu20.04

%labels
    Author IFB
    Version 0.3.0+singularity0

%post
    export DORADO_VERSION='0.3.0'
    export DEBIAN_FRONTEND=noninteractive
    apt-get -y -q  update && apt-get -y -q upgrade
    apt-get -y -q install wget tar

    # Dorado
    cd /usr/local/
    mkdir dorado
    wget -q https://cdn.oxfordnanoportal.com/software/analysis/dorado-${DORADO_VERSION}-linux-x64.tar.gz
    tar -zxf dorado-${DORADO_VERSION}-linux-x64.tar.gz
    mv dorado-${DORADO_VERSION}-linux-x64/* dorado/.

    # Cleaning
    rm -rf /usr/local/dorado-${DORADO_VERSION}-linux-x64.tar.gz
    apt-get -qq clean && rm -rf /var/lib/apt/lists/*

%environment
    export PATH=/usr/local/dorado/bin/:$PATH

%runscript
    echo "Nanopore Dorado requires a license agreement: https://github.com/nanoporetech/dorado/blob/master/LICENCE.txt"
    exec "$@"

%test
    export PATH=/usr/local/dorado/bin/:$PATH
    cd /tmp
    dorado download --model dna_r10.4.1_e8.2_400bps_hac@v4.1.0
